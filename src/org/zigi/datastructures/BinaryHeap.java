package org.zigi.datastructures;

import java.util.Arrays;

/**
 * Binární halda
 * 
 * @author zigi
 *
 */
public class BinaryHeap {
	private int[] array;
	private int size; // velikost haldy...je treba mit na pameti, ze indexujeme
						// od 1

	public BinaryHeap(int arraySize) {
		this.array = new int[arraySize + 1]; // na prvnim miste nebude nic
		this.size = 0;
	}

	public BinaryHeap(int[] source) {
		this.array = new int[source.length + 1]; // na prvnim miste nebude nic
		System.arraycopy(source, 0, array, 1, source.length);
		this.size = 0;
	}

	public void merge(BinaryHeap heap) {
		int[] newArray = new int[this.size + heap.size + 1];
		System.arraycopy(array, 1, newArray, 1, this.size);
		System.arraycopy(heap.array, 1, newArray, this.size + 1, heap.size);
		size = this.size + heap.size;
		array = newArray;
		heapify(newArray);
	}

	public int[] getAll() {
		return Arrays.copyOfRange(array, 1, this.size + 1);
	}

	public void insert(int i) {
		size++;
		int index = this.size;
		while (i < array[index / 2] && index != 0) { // dokud je nas prvek mensi
														// nez jeho otec
			array[index] = array[index / 2]; // pak otce posuneme o uroven niz
												// (cimz se nam mezera na
												// vlozeni posune o patro)
			index /= 2; // a opakujeme o uroven vys
		}
		array[index] = i; // o patro vys je jiz prvek nizsi, proto vlozime prvek
							// prave sem, vlastnost haldy byla obnovena
	}

	public int top() {
		if (getSize() == 0) {
			throw new IllegalStateException("halda je prazdna");
		}
		return array[1];
	}

	public int returnTop() {
		if (getSize() == 0) {
			throw new IllegalStateException("halda je prazdna");
		}
		int tmp = array[1];
		array[1] = array[this.size];
		size--;
		repairTop(this.size, 1);
		return tmp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 1; i <= this.size; i++) {
			builder.append(array[i]).append(" ");
		}
		return builder.toString();
	}

	public int getSize() {
		return size;
	}

	private void heapify(int[] array) {
		for (int i = array.length / 2; i > 0; i--) {
			repairTop(this.size, i);
		}
	}

	private void repairTop(int bottom, int topIndex) {
		int tmp = array[topIndex];
		int succ = topIndex * 2;
		if (succ < bottom && array[succ] > array[succ + 1]) {
			succ++;
		}
		while (succ <= bottom && tmp > array[succ]) {
			array[topIndex] = array[succ];
			topIndex = succ;
			succ = succ * 2;
			if (succ < bottom && array[succ] > array[succ + 1]) {
				succ++;
			}
		}
		array[topIndex] = tmp;
	}
}
