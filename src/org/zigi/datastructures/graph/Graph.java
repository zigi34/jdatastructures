package org.zigi.datastructures.graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.zigi.datastructures.graph.edges.Edge;
import org.zigi.datastructures.graph.nodes.Node;
import org.zigi.datastructures.graph.util.DijkstraGraphLength;

public abstract class Graph<T> implements Serializable {
	protected List<Node<T>> nodes = new LinkedList<Node<T>>();
	protected List<Edge<T>> edges = new LinkedList<Edge<T>>();
	private Class<T> genericType;

	public Graph(Class<T> type) {
		this.genericType = type;
	}

	public void addNode(T object) {
		Node<T> node = new Node<T>(object, this);
		node.setGraph(this);
		if (!nodes.contains(node)) {
			nodes.add(node);
		}
	}

	public double getAssortCoef() {
		double sum = 0.0;
		double m = getEdgeSize();
		for (T i : getNodes()) {
			for (T j : getNodes()) {
				double val = 0;
				if (getEdge(i, j) != null)
					val = 1;
				sum += (val - (getNeighborsCount(i) * getNeighborsCount(j))
						/ (2 * m))
						* (getNeighborsCount(i) * getNeighborsCount(j));
			}
		}

		double sum2 = 0.0;
		for (T i : getNodes()) {
			for (T j : getNodes()) {
				double val = 0;
				if (i.equals(j))
					val = getNeighborsCount(i);
				sum2 += (val - (getNeighborsCount(i) * getNeighborsCount(j))
						/ (2 * m))
						* (getNeighborsCount(i) * getNeighborsCount(j));
			}
		}
		return sum / sum2;
	}

	public void removeNode(T object) {
		Node<T> node = new Node<T>(object, this);
		if (nodes.contains(node)) {
			nodes.remove(node);
		}
		// create list with edges must be removed
		List<Edge<T>> remove = new LinkedList<Edge<T>>();
		for (Edge<T> edge : edges) {
			if (edge.containNode(node))
				remove.add(edge); // add to list for remove
		}
		// remove all edges, which is contain node
		for (Edge<T> edge : remove) {
			edges.remove(edge);
		}
	}

	/**
	 * Clear all nodes and edges
	 */
	public void clear() {
		nodes.clear();
		edges.clear();
	}

	/**
	 * 
	 * @param node1
	 * @param node2
	 */
	public abstract void addEdge(T node1, T node2);

	public abstract void addEdge(T node1, T node2, double length);

	public void addEdge(Edge<T> edge) {
		edges.add(edge);
	}

	public abstract void removeEdge(T node1, T node2);

	/**
	 * Remove edge from graph and can choose, if nodes stay. if edge not exist
	 * and withNode set true, nodes it will be removed also
	 * 
	 * @param node1
	 *            first node
	 * @param node2
	 *            second node
	 * @param withNodes
	 *            if it is true, it will be removed nodes too, else nodes stay
	 */
	public void removeEdge(T node1, T node2, boolean withNodes) {
		removeEdge(node1, node2);
		if (withNodes) {
			removeNode(node1);
			removeNode(node2);
		}
	}

	/**
	 * Determine whether node is isolated in graph
	 * 
	 * @param node
	 *            tested node
	 * @return true - it is isolated node, otherwise false
	 */
	public boolean isIsolated(T node) {
		Node<T> newNode = new Node<T>(node, this);
		if (nodes.contains(newNode)) {
			for (Edge<T> edge : edges) {
				if (edge.containNode(node))
					return false;
			}
		}
		return true;

	}

	public boolean containNode(T node) {
		Node<T> newNode = new Node<T>(node, this);
		if (nodes.contains(newNode))
			return true;
		return false;
	}

	public abstract boolean containEdge(T node1, T node2);

	public int getNodeSize() {
		return nodes.size();
	}

	public int getEdgeSize() {
		return edges.size();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Node<T> node : nodes) {
			sb.append(node.toString() + "\n");
		}
		sb.append("\n");
		for (Edge<T> edge : edges) {
			sb.append(edge.toString());
		}
		return sb.toString();
	}

	public List<T> getNodes() {
		List<T> result = new ArrayList<T>(nodes.size());
		for (Node<T> node : nodes) {
			result.add(node.getValue());
		}
		return result;
	}

	public int getIndexOfNode(T nodeValue) {
		return nodes.indexOf(new Node<T>(nodeValue, this));
	}

	public List<Edge<T>> getEdges() {
		return edges;
	}

	/**
	 * Průměrný stupeň <k> =
	 * 
	 * @return
	 */
	public double getAverageNodeDegree() {
		return 2 * getEdgeSize() / getNodeSize();
	}

	/**
	 * Diametr = nejdelší nejkratší cesta
	 * 
	 * @return
	 */
	public double getDiameter() {
		DijkstraGraphLength<T> dij = new DijkstraGraphLength<>();
		double maxVal = -1;
		for (T val : getNodes()) {
			List<Double> d = dij.getLengths(this, val);
			for (Double dou : d) {
				if (dou > maxVal)
					maxVal = dou;
			}
		}
		return maxVal;
	}

	/**
	 * Počet vrcholů zadaného stupně vrcholů
	 * 
	 * @param degree
	 *            stupeň vrcholu
	 * @return
	 */
	public int getDistributionNodeCount(int degree) {
		int count = 0;
		for (Node<T> n : nodes) {
			if (n.getNeighborsCount() == degree)
				count++;
		}
		return count;
	}

	/**
	 * Počet sousedů zadaného uzlu
	 * 
	 * @param node
	 *            uzel
	 * @return
	 */
	public int getNeighborsCount(Node<T> node) {
		int count = 0;
		for (Edge<T> e : getEdges()) {
			if (e.containNode(node))
				count++;
		}
		return count;
	}

	public int getNeighborsCount(T val) {
		return getNeighborsCount(new Node<T>(val, this));
	}

	public abstract Edge<T> getEdge(T node1, T node2);

	public List<Double> getBasicShortestPath(T fromVector) {
		// ceny k uzlům
		List<Double> cost = new LinkedList<Double>();
		// předposlední uzel na cestě
		List<Integer> prev = new LinkedList<Integer>();

		// inicializace
		for (int i = 0; i < nodes.size(); i++) {
			Node<T> node = nodes.get(i);
			if (node.getValue().equals(fromVector)) {
				prev.add(-1);
				cost.add(0.0);
			} else {
				cost.add(Double.MAX_VALUE);
				prev.add(0);
			}
		}
		// průchod všemi hranami s uložením stavu o změnách
		boolean isChanged = false;
		do {
			isChanged = false;
			// projdeme všechny hrany
			for (Edge<T> edge : edges) {
				int nodeIndex1 = nodes.indexOf(edge.getNode1());
				int nodeIndex2 = nodes.indexOf(edge.getNode2());
				if (cost.get(nodeIndex1) != Double.MAX_VALUE
						&& cost.get(nodeIndex1) + edge.getLengthValue() < cost
								.get(nodeIndex2)) {
					cost.set(nodeIndex2,
							cost.get(nodeIndex1) + edge.getLengthValue());
					prev.set(nodeIndex2, nodeIndex1);
					isChanged = true;
				} else if (cost.get(nodeIndex2) != Double.MAX_VALUE
						&& cost.get(nodeIndex2) + edge.getLengthValue() < cost
								.get(nodeIndex1)) {
					cost.set(nodeIndex1,
							cost.get(nodeIndex2) + edge.getLengthValue());
					prev.set(nodeIndex1, nodeIndex2);
					isChanged = true;
				}
			}
		} while (isChanged);
		return cost;
	}

	public void saveToFile(File file) {
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}

	public Graph<T> loadFromFile(File file) {
		Graph<T> graph = null;
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			graph = (Graph<T>) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return graph;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return graph;
		}
		return graph;
	}

	public void loadFromCSV(File file) throws Exception {
		if (file != null) {
			BufferedReader br = null;
			Graph<T> graph = new UndirectedGraph<>(getGenericType());
			try {
				br = new BufferedReader(new FileReader(file));
				String line = null;
				do {
					line = br.readLine();
					if (line == null)
						break;
					String[] values = line.split(";");
					if (getGenericType() == Integer.class) {
						addEdge(getGenericType().cast(
								Integer.parseInt(values[0])), getGenericType()
								.cast(Integer.parseInt(values[1])));
					} else {
						throw new Exception("Unknow generic type");
					}
				} while (line != null);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null)
					br.close();
			}
		}
	}

	public void saveToPajekFile(File file) {
		if (file != null) {
			PrintWriter ps = null;
			try {
				ps = new PrintWriter(file);
				ps.println("*Vertices " + getNodeSize());
				for (int i = 0; i < getNodeSize(); i++)
					ps.println(getNodes().get(i));
				ps.println("*edges");
				for (int i = 0; i < getEdgeSize(); i++) {
					Edge<T> edge = getEdges().get(i);
					ps.println(String.format("%s %s", edge.getNode1()
							.getValue(), edge.getNode2().getValue()));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if (ps != null)
					ps.close();
			}
		}
	}

	public void saveToNodeXL(File file) {
		if (file != null) {
			PrintWriter ps = null;
			try {
				ps = new PrintWriter(file);
				for (int i = 0; i < getEdgeSize(); i++) {
					Edge<T> edge = getEdges().get(i);
					ps.println(String.format("%s,%s", edge.getNode1()
							.getValue(), edge.getNode2().getValue()));
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if (ps != null)
					ps.close();
			}
		}
	}

	public Class<T> getGenericType() {
		return genericType;
	}
}
