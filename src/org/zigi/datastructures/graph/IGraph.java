package org.zigi.datastructures.graph;

import org.zigi.datastructures.graph.nodes.IVertex;

public interface IGraph {

	void AddVertex(IVertex vertex);

	void AddEdge(IVertex vertex1, IVertex vertex2);
}
