package org.zigi.datastructures.graph;

public interface IGraphElement<T> {
	/**
	 * Is this object Node?
	 * 
	 * @return return true, if it is node object otherwise false
	 */
	boolean isNode();

	/**
	 * Is this object Edge
	 * 
	 * @return return true, if it is edge object otherwise false
	 */
	boolean isEdge();
}
