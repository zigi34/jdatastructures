package org.zigi.datastructures.graph;

public interface IMetadata {
	void SetValue(String name, Object value);

	Object GetValue(String name);
}
