/**
 * 
 */
package org.zigi.datastructures.graph;

import org.zigi.datastructures.graph.edges.Edge;
import org.zigi.datastructures.graph.edges.UndirectedEdge;
import org.zigi.datastructures.graph.nodes.Node;

/**
 * @author zigi
 *
 */
public class UndirectedGraph<T> extends Graph<T> {

	public UndirectedGraph(Class<T> generitType) {
		super(generitType);
	}

	@Override
	public void addEdge(T node1, T node2) {
		Node<T> n1 = new Node<T>(node1, this);
		Node<T> n2 = new Node<T>(node2, this);
		Edge<T> edge = new UndirectedEdge<T>(node1, node2, this);
		edge.setGraph(this);
		if (!edges.contains(edge)) {
			edges.add(edge);
		}
		if (!nodes.contains(n1))
			nodes.add(n1);
		if (!nodes.contains(n2))
			nodes.add(n2);
	}

	@Override
	public boolean containEdge(T node1, T node2) {
		Edge<T> newEdge = new UndirectedEdge<T>(node1, node2, this);
		if (edges.contains(newEdge))
			return true;
		return false;
	}

	@Override
	public void removeEdge(T node1, T node2) {
		Edge<T> newEdge = new UndirectedEdge<>(node1, node2, this);
		if (edges.contains(newEdge))
			edges.remove(newEdge);
	}

	@Override
	public Edge<T> getEdge(T node1, T node2) {
		Edge<T> edge = new UndirectedEdge<T>(node1, node2, this);
		if (edges.indexOf(edge) != -1)
			return edges.get(edges.indexOf(edge));
		return null;
	}

	@Override
	public void addEdge(T node1, T node2, double length) {
		Edge<T> edge = new UndirectedEdge<T>(node1, node2, length, this);
		if (!edges.contains(edge))
			edges.add(edge);
		if (!nodes.contains(edge.getNode1()))
			nodes.add(edge.getNode1());
		if (!nodes.contains(edge.getNode2()))
			nodes.add(edge.getNode2());
	}
}
