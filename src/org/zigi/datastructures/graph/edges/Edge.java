package org.zigi.datastructures.graph.edges;

import java.io.Serializable;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.IGraphElement;
import org.zigi.datastructures.graph.nodes.Node;

public abstract class Edge<T> implements IGraphElement<T>, Serializable {
	protected Node<T> node1;
	protected Node<T> node2;
	protected double lengthValue;
	private Graph<T> graph;

	public Edge(Node<T> node1, Node<T> node2, Graph<T> graph) {
		this.node1 = node1;
		this.node2 = node2;
		this.lengthValue = 1.0;
		this.graph = graph;
	}

	public Edge(Node<T> node1, Node<T> node2, double length, Graph<T> graph) {
		this.node1 = node1;
		this.node2 = node2;
		this.lengthValue = length;
		this.graph = graph;
	}

	public Edge(T node1, T node2, Graph<T> graph) {
		this.node1 = new Node<T>(node1, graph);
		this.node2 = new Node<T>(node2, graph);
		this.lengthValue = 1.0;
		this.graph = graph;
	}

	public Edge(T node1, T node2, double length, Graph<T> graph) {
		this.node1 = new Node<T>(node1, graph);
		this.node2 = new Node<T>(node2, graph);
		this.lengthValue = 1.0;
		this.graph = graph;
		this.lengthValue = length;
	}

	/**
	 * 
	 * @return
	 */
	public Node<T> getNode1() {
		return node1;
	}

	/**
	 * Get second node of edge
	 * 
	 * @return
	 */
	public Node<T> getNode2() {
		return node2;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Edge) {
			Edge<T> e = (Edge<T>) obj;
			if (e.getNode1().equals(getNode1())
					&& e.getNode2().equals(getNode2()))
				return true;
			else if (e.getNode1().equals(getNode2())
					&& e.getNode2().equals(getNode1()))
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "(" + getNode1().toString() + ";" + getNode2().toString() + ")["
				+ getLengthValue() + "]";
	}

	/**
	 * Determines whether node is contained in the edge
	 * 
	 * @param node
	 *            node of the graph
	 * @return
	 */
	public boolean containNode(Node<T> node) {
		if (getNode1().equals(node) || getNode2().equals(node))
			return true;
		return false;
	}

	/**
	 * Determines whether node is contained in the edge
	 * 
	 * @param node
	 *            node of the graph
	 * @return
	 */
	public boolean containNode(T node) {
		Node<T> item = new Node<T>(node, null);
		if (getNode1().equals(item) || getNode2().equals(item))
			return true;
		return false;
	}

	public boolean isEdge() {
		return true;
	}

	public boolean isNode() {
		return false;
	}

	public Graph<T> getGraph() {
		return graph;
	}

	/**
	 * Set reference to graph
	 * 
	 * @param graph
	 *            graph reference
	 */
	public void setGraph(Graph<T> graph) {
		this.graph = graph;
		if (getNode1() != null)
			getNode1().setGraph(graph);
		if (getNode2() != null)
			getNode2().setGraph(graph);
	}

	public double getLengthValue() {
		return lengthValue;
	}

	public void setLengthValue(double lengthValue) {
		if (lengthValue < 0)
			throw new IllegalArgumentException("Délka hrany nemůže být záporná");
		this.lengthValue = lengthValue;
	}
}
