package org.zigi.datastructures.graph.edges;

import org.zigi.datastructures.graph.IMetadata;
import org.zigi.datastructures.graph.nodes.IVertex;

public interface IEdge {

	long Id();

	IMetadata Metadata();

	IVertex VertexA();

	IVertex VertexB();

	Boolean Contains(IVertex vertex);
}
