package org.zigi.datastructures.graph.edges;

public interface IEdgeFilter {
	Boolean IsFiltered(IEdge edge);
}
