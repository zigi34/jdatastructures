package org.zigi.datastructures.graph.edges;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.nodes.Node;

public class UndirectedEdge<T> extends Edge<T> {

	public UndirectedEdge(Node<T> node1, Node<T> node2, Graph<T> graph) {
		super(node1, node2, graph);
	}

	public UndirectedEdge(T node1, T node2, Graph<T> graph) {
		super(node1, node2, graph);
	}

	public UndirectedEdge(T node1, T node2, double length, Graph<T> graph) {
		super(node1, node2, length, graph);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Edge) {
			Edge<T> edge = (Edge<T>) obj;
			if (edge.getNode1().equals(getNode1())
					&& edge.getNode2().equals(getNode2())
					|| edge.getNode1().equals(getNode2())
					&& edge.getNode2().equals(getNode1()))
				return true;
		}
		return false;
	}
}
