package org.zigi.datastructures.graph.nodes;

import java.util.HashSet;

import org.zigi.datastructures.graph.IMetadata;
import org.zigi.datastructures.graph.edges.IEdge;
import org.zigi.datastructures.graph.edges.IEdgeFilter;

public interface IVertex {
	long Id();

	void AddEdge(IEdge edge);

	IMetadata Metadata();

	HashSet<IEdge> GetEdges();

	HashSet<IEdge> GetEdgesBy(IEdgeFilter filter);

	HashSet<IVertex> GetAdjacentVertices();

	HashSet<IVertex> GetAdjacentVerticesBy(IVertexFilter filter);
}
