package org.zigi.datastructures.graph.nodes;

public interface IVertexFilter {
	Boolean IsFiltered(IVertex vertex);
}
