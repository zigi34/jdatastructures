package org.zigi.datastructures.graph.nodes;

import java.io.Serializable;

import org.zigi.datastructures.graph.Graph;

public class Node<T> implements Serializable {
	private T value;
	private Graph<T> graph;

	public Node(T value, Graph<T> graph) {
		this.value = value;
		this.graph = graph;
	}

	/**
	 * Get value of node
	 * 
	 * @return
	 */
	public T getValue() {
		return this.value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node) {
			Node<T> vertex = (Node<T>) obj;
			if (vertex.getValue().equals(value))
				return true;
		}
		return false;
	}

	/**
	 * Set reference to controll graph
	 * 
	 * @param graph
	 *            graph, which is using vertex
	 */
	public void setGraph(Graph<T> graph) {
		this.graph = graph;
	}

	/**
	 * Get reference to graph
	 * 
	 * @return
	 */
	public Graph<T> getGraph() {
		return graph;
	}

	/**
	 * Is it isolated vertex?
	 * 
	 * @return return true, if it is isolated, otherwise false
	 */
	public boolean isIsolated() {
		if (getGraph() != null) {
			return getGraph().isIsolated(this.getValue());
		}
		return true;
	}

	public boolean isEdge() {
		return false;
	}

	public boolean isVertex() {
		return true;
	}

	public int getNeighborsCount() {
		return getGraph().getNeighborsCount(this);
	}

	@Override
	public String toString() {
		return getValue().toString();
	}
}
