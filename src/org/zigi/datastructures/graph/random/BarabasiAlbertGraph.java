package org.zigi.datastructures.graph.random;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;

public class BarabasiAlbertGraph {
	private static Random random = new Random();

	/**
	 * Vygenerování náhodného grafu podle Barabasi Alberta s modelem B (se
	 * zvýhodněním uzlů s vyšším stupněm)
	 * 
	 * @param initNodeConn
	 *            počáteční počet uzlů
	 * @param maxNodes
	 *            maximální počet uzlů
	 * @return
	 */
	public static Graph<Integer> createGraph(int initNodeConn, int maxNodes) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);
		// Vygenerujeme počáteční vrcholy
		for (int i = 0; i < initNodeConn - 1; i++)
			graph.addNode(i + 1);

		// spojíme vrcholy s novým uzlem
		int init = graph.getNodeSize() + 1;
		for (int i = 0; i < initNodeConn - 1; i++)
			graph.addEdge(init, i + 1);

		int t = 1;
		List<Integer> vert = new LinkedList<Integer>();
		while (graph.getNodeSize() < maxNodes) {
			vert.clear();
			vert.addAll(graph.getNodes());
			init += 1;
			graph.addNode(init);
			int connected = 0;
			while (connected < initNodeConn - 3) {
				if (vert.size() == 0)
					break;
				Integer ver = vert.remove(random.nextInt(vert.size()));
				if (mayConnect(graph, ver)) {
					graph.addEdge(init, ver);
					connected++;
				}
			}
			if (t == 1 || t == 10 || t == 100 || t == 1000 || t == 10000) {
				System.out.println("-----------------------------------t=" + t
						+ "--------------------------------------");
				for (int i = 0; i < 100; i++) {
					int count = graph.getDistributionNodeCount(i);
					System.out.println(i + ":" + count);
				}
			}
			t++;
		}
		System.out.println("-----------------------------------t=" + t
				+ "--------------------------------------");
		for (int i = 0; i < 100; i++) {
			int count = graph.getDistributionNodeCount(i);
			System.out.println(i + ":" + count);
		}
		return graph;
	}

	/**
	 * Vygenerování náhodného grafu podle Barabasi Alberta s modelem A
	 * (konstantní pravděpodobnost)
	 * 
	 * @param prob
	 *            pravděpodobnost na spojení s uzlem
	 * @param initNodeConn
	 *            počáteční uzly
	 * @param maxNodes
	 *            maximum uzlů
	 * @return
	 */
	public static Graph<Integer> createGraph(double prob, int initNodeConn,
			int maxNodes) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);
		// Vygenerujeme počáteční vrcholy
		for (int i = 0; i < initNodeConn; i++)
			graph.addNode(i + 1);

		// spojíme vrcholy s novým uzlem
		int init = graph.getNodeSize() + 1;
		for (int i = 0; i < initNodeConn; i++)
			graph.addEdge(init, i + 1);

		List<Integer> vert = new LinkedList<Integer>();
		while (graph.getNodeSize() < maxNodes) {
			vert.clear();
			vert.addAll(graph.getNodes());
			init += 1;
			graph.addNode(init);
			int connected = 0;
			while (connected < initNodeConn) {
				if (vert.size() == 0)
					break;
				Integer ver = vert.remove(random.nextInt(vert.size()));
				if (mayConnect(graph, prob)) {
					graph.addEdge(init, ver);
					connected++;
				}
			}
		}
		return graph;
	}

	private static boolean mayConnect(Graph<Integer> graph, double prob) {
		return prob < random.nextDouble();
	}

	private static boolean mayConnect(Graph<Integer> graph, Integer node) {
		double prob = graph.getNeighborsCount(node)
				/ (graph.getEdgeSize() * 2.0);
		return prob < random.nextDouble();
	}
}
