package org.zigi.datastructures.graph.random;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;
import org.zigi.datastructures.graph.edges.UndirectedEdge;

public class ErdosRenyiGraph {

	private static Random random = new Random();

	public static Graph<Integer> createGraph(int nodeCount, int edgeCount) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);
		for (int i = 0; i < nodeCount; i++)
			graph.addNode(i + 1);

		List<UndirectedEdge<Integer>> edges = new LinkedList<UndirectedEdge<Integer>>();
		for (int i = 0; i < nodeCount; i++)
			for (int j = i + 1; j < nodeCount; j++)
				edges.add(new UndirectedEdge<Integer>(i + 1, j + 1, graph));

		while (graph.getEdgeSize() < edgeCount) {
			int index = random.nextInt(edges.size());
			UndirectedEdge<Integer> edge = edges.get(index);
			graph.addEdge(edge);
			edges.remove(index);
		}
		return graph;
	}

	public static Graph<Integer> createGraph(int nodeCount,
			double edgeProbability) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);
		for (int i = 0; i < nodeCount; i++)
			graph.addNode(i + 1);

		for (int i = 0; i < nodeCount; i++)
			for (int j = i + 1; j < nodeCount; j++) {
				double rand = random.nextDouble();
				if (rand <= edgeProbability) {
					graph.addEdge(new UndirectedEdge<Integer>(i + 1, j + 1,
							graph));
				}
			}
		return graph;
	}
}
