package org.zigi.datastructures.graph.random;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;
import org.zigi.datastructures.graph.edges.UndirectedEdge;

public class KlemmEguiluzRandomGraph {
	private static Random random = new Random();

	public static Graph<Integer> createGraph(double deactive_prob,
			int init_node, int maxNodes) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);

		// add initial nodes and edges
		for (int i = 0; i < init_node; i++)
			graph.addNode(i + 1);
		for (int i = 0; i < init_node; i++)
			for (int j = i + 1; j < init_node; j++)
				graph.addEdge(new UndirectedEdge<Integer>(i + 1, j + 1, graph));

		// add nodes to active list
		List<Integer> activeList = new LinkedList<Integer>();
		for (int vertex : graph.getNodes())
			activeList.add(vertex);

		int actualNode = init_node + 1;
		// loop while max nodes exceeded
		while (graph.getNodeSize() < maxNodes) {
			// add new node
			graph.addNode(actualNode);

			// connect with active nodes
			for (int activeNode : activeList)
				graph.addEdge(new UndirectedEdge<Integer>(activeNode,
						actualNode, graph));

			// add node to active list
			activeList.add(actualNode);

			// deactive one node
			deactiveNode(activeList, graph, deactive_prob);
			actualNode++;
		}
		return graph;
	}

	private static void deactiveNode(List<Integer> activeNodes,
			Graph<Integer> graph, double a) {
		boolean deactived = false;
		double num = 0;
		for (int active : activeNodes) {
			int neigh = graph.getNeighborsCount(active);
			num += 1.0 / (a + neigh);
		}
		num = Math.pow(num, -1.0);
		double[] prob = new double[activeNodes.size()];
		for (int i = 0; i < activeNodes.size(); i++) {
			prob[i] = num / (a + graph.getNeighborsCount(activeNodes.get(i)));
		}
		int index = 0;
		while (!deactived) {
			double rand = random.nextDouble();
			if (rand < prob[index]) {
				activeNodes.remove(index);
				break;
			}
			index = (index + 1) % activeNodes.size();
		}
	}
}
