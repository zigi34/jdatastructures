package org.zigi.datastructures.graph.random;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;

public class UCRGraph {
	public static Random random = new Random();
	public static int MAX_ITERATION = 5000000;

	public static Graph<Integer> createGraph(int nodes, double p, double alpha) {

		List<Integer> distribution = new LinkedList<>();
		Graph<Integer> result = new UndirectedGraph<>(Integer.class);
		int maxEdges = (int) (nodes * (nodes - 1) / 2 * p);

		for (int i = 0; i < nodes; i++)
			result.addNode(i + 1);

		int poc = 0;
		int vertex = 1;
		Random r = new Random();
		while (poc < maxEdges * 4) {
			int cis = getPowerLawX(alpha);
			if (cis > nodes)
				continue;
			if (cis == 0)
				continue;
			for (int i = 0; i < cis; i++)
				distribution.add(vertex);
			poc += cis;
			vertex = (vertex % nodes) + 1;
		}
		if (distribution.size() % 2 == 1)
			distribution.add(1);
		while (result.getEdgeSize() < maxEdges) {
			int randVal = r.nextInt(distribution.size());
			int val1 = distribution.remove(randVal);
			int randVal2 = r.nextInt(distribution.size());
			int val2 = distribution.remove(randVal2);

			if (val1 == val2) {
				distribution.add(val1);
				distribution.add(val2);
			} else {
				result.addEdge(val1, val2);
			}
		}
		return result;
	}

	public static List<Integer> createDistribution(int maxIteration,
			double alpha) {
		List<Integer> distributions = new LinkedList<>();
		for (int i = 0; i < maxIteration; i++) {
			int cislo = getPowerLawX(alpha);
			distributions.add(cislo);
		}
		return distributions;
	}

	private static int getPowerLawX(double alpha) {
		return (int) Math.floor(Math.pow(1 - random.nextDouble(), -1
				/ (alpha - 1)));
	}

	private UCRGraph() {

	}
}
