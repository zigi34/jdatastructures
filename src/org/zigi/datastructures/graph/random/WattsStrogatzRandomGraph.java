package org.zigi.datastructures.graph.random;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;
import org.zigi.datastructures.graph.edges.Edge;
import org.zigi.datastructures.graph.nodes.Node;

public class WattsStrogatzRandomGraph {

	private static Random random = new Random();

	public static Graph<Integer> createGraph(double beta, int kMeans,
			int maxNodes) {
		Graph<Integer> graph = new UndirectedGraph<Integer>(Integer.class);

		// add nodes
		for (int i = 0; i < maxNodes; i++)
			graph.addNode(i + 1);

		// circled graph
		for (int i = 0; i < maxNodes; i++)
			for (int j = 0; j < maxNodes; j++) {
				if (mayConnect(maxNodes, kMeans, i, j))
					graph.addEdge(i + 1, j + 1);
			}

		// re-wiring graph
		List<Edge<Integer>> edges = new LinkedList<>();
		for (int i = 0; i < graph.getEdgeSize(); i++) {
			Edge<Integer> e = graph.getEdges().get(i);
			if (e.getNode1().getValue() < e.getNode2().getValue()
					&& random.nextDouble() < beta) {
				edges.addAll(graph.getEdges());

				// odstraníme všechny hrany, které by mohly způsobit duplicitu
				Node<Integer> n1 = e.getNode1();
				for (Edge<Integer> edge : graph.getEdges())
					if (edge.containNode(n1.getValue()))
						edges.remove(edge);

				// přidáme zpět hranu původní
				edges.add(e);

				// Nyní vybereme náhraní hranu z povolených hran
				Edge<Integer> rewired = edges.get(random.nextInt(edges.size()));

				// odstraníme starou a přidáme novou hranu
				graph.removeEdge(e.getNode1().getValue(), e.getNode2()
						.getValue());
				graph.addEdge(e.getNode1().getValue(), rewired.getNode2()
						.getValue());

				edges.clear();
			}
		}
		return graph;
	}

	private static boolean mayConnect(int maxNodes, int kMeans, int i, int j) {
		int cis = Math.abs(i - j) % (maxNodes - kMeans / 2 - 1);
		if (0 < cis && cis <= kMeans / 2)
			return true;
		return false;
	}
}
