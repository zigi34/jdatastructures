package org.zigi.datastructures.graph.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.zigi.datastructures.graph.Graph;

public class DijkstraGraphLength<T> implements IGraphLength<T> {

	@Override
	public List<Double> getLengths(Graph<T> graph, T from) {
		List<T> closed = new LinkedList<T>();
		List<T> unvisited = new LinkedList<T>();
		List<Double> distances = new ArrayList<Double>(graph.getNodeSize());
		for (T node : graph.getNodes())
			distances.add(Double.MAX_VALUE);

		unvisited.addAll(graph.getNodes());
		unvisited.remove(from);
		closed.add(from);
		distances.set(graph.getIndexOfNode(from), 0.0);

		while (closed.size() != graph.getNodeSize()) {
			for (T curr : closed) {

				for (T next : unvisited) {
					if (graph.containEdge(curr, next)
							&& (unvisited.contains(curr) || unvisited
									.contains(next))) {
						int fromIndex = graph.getIndexOfNode(curr);
						int nextIndex = graph.getIndexOfNode(next);
						double dist = distances.get(fromIndex)
								+ graph.getEdge(curr, next).getLengthValue();
						if (dist < distances.get(nextIndex)) {
							distances.set(nextIndex, dist);
						}
					}
				}
			}
			T best = null;
			double bestVal = Double.MAX_VALUE;
			for (T node : unvisited) {
				int index = graph.getIndexOfNode(node);
				if (distances.get(index) < bestVal) {
					bestVal = distances.get(index);
					best = node;
				}
			}

			closed.add(best);
			unvisited.remove(best);
		}

		return distances;
	}
}
