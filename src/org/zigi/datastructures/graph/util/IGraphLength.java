package org.zigi.datastructures.graph.util;

import java.util.List;

import org.zigi.datastructures.graph.Graph;

public interface IGraphLength<T> {
	/**
	 * Získání všech vzdáleností v grafu z vybraného uzlu
	 * 
	 * @param graph
	 *            graf, ve kterém se počítají vzdálenosti
	 * @param from
	 *            uzel, od kterého se počítají vzdálenosti
	 * @return
	 */
	List<Double> getLengths(Graph<T> graph, T from);
}
