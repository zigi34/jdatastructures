package org.zigi.datastructures.junit;

import org.junit.Assert;
import org.junit.Test;
import org.zigi.datastructures.graph.Graph;
import org.zigi.datastructures.graph.UndirectedGraph;
import org.zigi.datastructures.graph.edges.Edge;
import org.zigi.datastructures.graph.edges.UndirectedEdge;
import org.zigi.datastructures.graph.nodes.Node;

public class GraphTest {
	@Test
	public void isolatedNodeTest() {
		Node<Integer> node = new Node<Integer>(15, null);
		Assert.assertTrue(node.isIsolated());
	}

	@Test
	public void isIsolatedInEdgeTest() {
		Node<Integer> node1 = new Node<Integer>(10, null);
		Node<Integer> node2 = new Node<Integer>(20, null);
		Edge<Integer> edge = new UndirectedEdge<Integer>(node1, node2, null);
		Assert.assertTrue(node1.isIsolated());
	}

	@Test
	public void containNodeTest() {
		Edge<Integer> edge2 = new UndirectedEdge<Integer>(5, 6, null);
		Assert.assertTrue(edge2.containNode(5));
	}

	@Test
	public void graphContainNodeTest() {
		Graph<Integer> g = new UndirectedGraph<Integer>(Integer.class);
		g.addNode(1);
		g.addNode(2);
		g.addNode(3);
		g.addNode(4);
		Assert.assertTrue(g.isIsolated(1));
		Assert.assertTrue(g.isIsolated(2));
		Assert.assertTrue(g.isIsolated(3));
		Assert.assertTrue(g.isIsolated(4));
	}

	@Test
	public void graphContainEdgeTest() {
		Graph<Integer> g = new UndirectedGraph<Integer>(Integer.class);
		g.addNode(1);
		g.addNode(2);
		g.addEdge(1, 2);
		Assert.assertTrue(g.containEdge(2, 1));
		Assert.assertTrue(g.containEdge(1, 2));
		g.addEdge(3, 8);
		Assert.assertTrue(g.containEdge(8, 3));
		Assert.assertFalse(g.containEdge(2, 3));
		Assert.assertTrue(g.containNode(3));
	}

	@Test
	public void graphRemoveEdgeTest() {
		Graph<Integer> g = new UndirectedGraph<Integer>(Integer.class);
		g.addEdge(1, 2);
		g.addEdge(3, 8);
		g.removeEdge(2, 1);
		Assert.assertFalse(g.containEdge(2, 1));
		Assert.assertTrue(g.containNode(1));
		Assert.assertTrue(g.containNode(2));
		g.removeEdge(1, 2, true);
		Assert.assertFalse(g.containEdge(2, 1));
		Assert.assertFalse(g.containNode(1));
		Assert.assertFalse(g.containNode(2));
	}
}
